#!/bin/bash

DIRNAME=$(dirname $0)

test -d $DIRNAME/../data/csv || mkdir -p $DIRNAME/../data/csv

test -n "${MAXMIND_LICENSE_KEY}" && cd $DIRNAME/../data/csv && {
  for csv in GeoLite2-ASN-CSV GeoLite2-City-CSV GeoLite2-Country-CSV
  do
    rm -rf ${csv}*
    curl -s -o ${csv}.zip "https://download.maxmind.com/app/geoip_download?edition_id=${csv}&license_key=${MAXMIND_LICENSE_KEY}&suffix=zip" \
      && curl -s -o ${csv}.zip.sha256 "https://download.maxmind.com/app/geoip_download?edition_id=${csv}&license_key=${MAXMIND_LICENSE_KEY}&suffix=zip.sha256" \
      && unzip -q ${csv}.zip \
      && rm -f ${csv}.zip ${csv}.zip.sha256
  done
}