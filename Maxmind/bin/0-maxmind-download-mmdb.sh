#!/bin/bash

DIRNAME=$(dirname $0)

test -d $DIRNAME/../data/mmdb || mkdir -p $DIRNAME/../data/mmdb

test -n "${MAXMIND_LICENSE_KEY}" && cd $DIRNAME/../data/mmdb && {
  for mmdb in GeoLite2-ASN GeoLite2-City GeoLite2-Country
  do
    rm -rf ${mmdb}*
    curl -s -o ${mmdb}.tar.gz "https://download.maxmind.com/app/geoip_download?edition_id=${mmdb}&license_key=${MAXMIND_LICENSE_KEY}&suffix=tar.gz" \
      && curl -s -o ${mmdb}.tar.gz.sha256 "https://download.maxmind.com/app/geoip_download?edition_id=${mmdb}&license_key=${MAXMIND_LICENSE_KEY}&suffix=tar.gz.sha256" \
      && tar -xzf ${mmdb}.tar.gz \
      && rm -f ${mmdb}.tar.gz ${mmdb}.tar.gz.sha256
  done
}