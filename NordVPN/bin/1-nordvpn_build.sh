#!/bin/bash

DIRNAME=$(dirname $0)

test -d $DIRNAME/../data || mkdir $DIRNAME/../data

while read c
do
  for i in $(seq 1 5000)
  do
    echo "$c$i.nordvpn.com"
  done
done < /home/bast/Git/External/nordvpn-ip-tool/lists/avail-country-list.txt > $DIRNAME/../data/nordvpn.domains
