#!/bin/bash

DIRNAME=$(dirname $0)

cat $DIRNAME/../data/nordvpn.domains | ~/bin/zdns A -udp-only -name-servers "9.9.9.9,8.8.8.8,8.8.4.4,1.1.1.1" > $DIRNAME/../data/nordvpn.domains.resolv.json
