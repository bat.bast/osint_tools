#!/bin/bash

DIRNAME=$(dirname $0)

(test -d $DIRNAME/../data/firehol/blocklist-ipsets \
  && cd $DIRNAME/../data/firehol/blocklist-ipsets \
  && git reset --hard \
  && git clean -fd \
  && git pull --rebase) \
|| (mkdir -p $DIRNAME/../data/firehol \
  && cd $DIRNAME/../data/firehol \
  && git clone https://github.com/firehol/blocklist-ipsets.git)