#!/bin/bash

DIRNAME=$(dirname $0)

test -d $DIRNAME/../data || mkdir $DIRNAME/../data

for t in http https socks4 socks5
do
  curl -s "https://www.proxy-list.download/api/v1/get?type=$t" > $DIRNAME/../data/www.proxy-list.download.$t
done
