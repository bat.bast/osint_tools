#!/bin/bash

sqlitefile=~/facebook_data_breach_042021/data/facebook_breach_202104.sqlite3
clean_solr=~/facebook_data_breach_042021/bin/initSOLRFBInstance.sh

echo "[$(date)] ---- START"
#$clean_solr ; sleep 10
curl -v "http://localhost:8983/solr/facebook_breach_2021/dataimport?command=full-import"

continue=0
while [ $continue -eq 0 ]
do
  echo "[$(date)]"
  curl -s "http://localhost:8983/solr/facebook_breach_2021/dataimport?command=status"|jq .statusMessages
  curl -s "http://localhost:8983/solr/facebook_breach_2021/dataimport?command=status"|grep importResponse|grep "running"
  if [ $? -eq 0 ] ; then
    continue=0
    sleep 10
  else
    continue=1
  fi
done
echo "[$(date)] ---- END"
