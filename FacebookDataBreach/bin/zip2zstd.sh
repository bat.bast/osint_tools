#!/bin/bash

zipfile=$1

echo "$zipfile --> $(basename $zipfile .zip).tar.zst..."
unzip -l $zipfile && unzip -x $zipfile && {
  tar -cvf - $(basename $zipfile .zip).txt | zstd -vvv --ultra -19 -T0 -o $(basename $zipfile .zip).tar.zst
} && rm $zipfile $(basename $zipfile .zip).txt && echo -e "\t+++++ $zipfile --> $(basename $zipfile .zip).tar.zst [OK]" || echo -e "\t----- $zipfile --> $(basename $zipfile .zip).tar.zst [KO]"
