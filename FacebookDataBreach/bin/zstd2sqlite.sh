#!/bin/bash

COLS="PHONE,FB_ID,LASTNAME,FIRSTNAME,GENDER,LOCATION1,LOCATION2,STATUS,COMPANY,EMAIL,BIRTHDAY"

sqlitefile=~/facebook_data_breach_042021/data/facebook_breach_202104.sqlite3
data_format=~/facebook_data_breach_042021/bin/data_format.txt
sql_import=~/facebook_data_breach_042021/bin/import_csv2db.sql

zstdfile=$1

echo "[$(date)] ---- $zstdfile ---- START"
echo "$zstdfile --> $(basename $sqlitefile)..."
tar --zstd -xvf $zstdfile \
    && country=$(basename $zstdfile .tar.zst) \
    && country_file=${country}.txt \
    && dos2unix $country_file \
    && echo -e "\t+++++ $(wc -l $country_file)" \
    && sep=$(grep "^$country" $data_format|cut -d"#" -f2) \
    && columns=$(grep "^$country" $data_format|cut -d"#" -f3|tr ";" "$sep") \
    && sed -i "1s/^/$columns\n/" $country_file \
    && ok_columns=$(for col in $(echo $COLS|tr "," "\n"); do echo $columns|grep "$col">/dev/null; if [ $? -eq 0 ] ; then echo "$col"; fi; done | tr "\n" ",") \
    && sed "s/__SEP__/$sep/g; s/__CSV__/$country_file/g; s/__COLS__/$ok_columns/g" $sql_import > local_sql_import.sql \
    && sqlite3 -init local_sql_import.sql $sqlitefile .quit \
    && rm -f local_sql_import.sql $country_file \
    && echo -e "\t+++++ $zstdfile --> $(basename $sqlitefile) [OK]" \
    || echo -e "\t----- $zstdfile --> $(basename $sqlitefile) [KO]"
echo "[$(date)] ---- $zstdfile ---- END"
