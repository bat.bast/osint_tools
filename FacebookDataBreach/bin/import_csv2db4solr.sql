.load /home/bast/facebook_data_breach_042021/sqlite-ext/sqlite3-vsv.so

--SELECT "Table SOLR - Total records = "||count(*) FROM SOLR;

create virtual table temp.vsv using vsv(filename="__CSV__", header=on, nulls=on, fsep=";");

insert into SOLR select * from temp.vsv;

DELETE FROM SOLR WHERE FB_ID IS NULL;

SELECT "Table SOLR - New Total records = "||count(*) FROM SOLR;
