#!/bin/bash

txt="$*"

ls -lh $txt && tar -cvf - $txt | zstd -vvv --ultra -19 -T0 -o $(basename $1 .txt).tar.zst && rm $txt
