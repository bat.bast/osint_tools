#!/bin/bash

sqlitefile=~/facebook_data_breach_042021/data/facebook_breach_202104.sqlite3
data_format=~/facebook_data_breach_042021/bin/data_format_solr.txt
sql_import=~/facebook_data_breach_042021/bin/import_csv2db4solr.sql
head_log=~/facebook_data_breach_042021/data/heads.log

zstdfile=$1

echo "[$(date)] ---- $zstdfile ---- START"

echo "$zstdfile --> $(basename $sqlitefile)..."
tar --zstd -xvf $zstdfile \
    && country=$(basename $zstdfile .tar.zst) \
    && country_file=${country}.txt \
    && dos2unix $country_file \
    && echo "########### $country ###########" >> $head_log \
    && head -20 $country_file >> $head_log \
    && echo "" >> $head_log \
    && echo -e "\t+++++ $(wc -l $country_file)" \
    && sep=$(grep "^$country" $data_format|cut -d"#" -f2) \
    && column_fb_id=$(grep "^$country" $data_format|cut -d"#" -f3) \
    && cut -d"$sep" -f$column_fb_id $country_file > $country_file.fb_id \
    && sed 's/"//; s/,None,/,,/; s/:None:/::/; s/^/"/; s/$/"/' $country_file > $country_file.raw_data \
    && echo "FB_ID;RAW_DATA" > $country_file \
    && paste $country_file.fb_id $country_file.raw_data -d ";" >> $country_file \
    && sed "s/__CSV__/$country_file/g" $sql_import > local_sql_import.sql \
    && sqlite3 -init local_sql_import.sql $sqlitefile .quit \
    && rm -f local_sql_import.sql $country_file $country_file.raw_data $country_file.fb_id \
    && echo -e "\t+++++ $zstdfile --> $(basename $sqlitefile) [OK]" \
    || echo -e "\t----- $zstdfile --> $(basename $sqlitefile) [KO]"
echo "[$(date)] ---- $zstdfile ---- END"
