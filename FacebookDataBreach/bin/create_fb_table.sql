CREATE TABLE FB2021 (
	PHONE INTEGER,
	FB_ID INTEGER,
	LASTNAME TEXT,
	FIRSTNAME TEXT,
	GENDER TEXT,
	LOCATION1 TEXT,
	LOCATION2 TEXT,
	STATUS TEXT,
	COMPANY TEXT,
	EMAIL TEXT,
	BIRTHDAY TEXT
    );

--CREATE INDEX FB2021_PHONE_IDX ON FB2021 (PHONE);
--CREATE INDEX FB2021_LASTNAME_IDX ON FB2021 (LASTNAME);
--CREATE INDEX FB2021_EMAIL_IDX ON FB2021 (EMAIL);
--CREATE INDEX FB2021_COMPANY_IDX ON FB2021 (COMPANY);


CREATE TABLE SOLR (
	FB_ID INTEGER,
	RAW_DATA TEXT
    );
