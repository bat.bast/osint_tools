import unittest


class FireHOLTest(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        from BlockListAnalyzer import BlockListAnalyzer
        bla = BlockListAnalyzer('~/Git/Gitlab/OSINT-Tools/FireHOLDataSQLite/test/.firehol-data-sqlite.ini')
        bla.analyze_blocklist()

    def test_ip4(self):
        from BlockListAnalyzer import BlockListAnalyzer
        bla = BlockListAnalyzer('~/Git/Gitlab/OSINT-Tools/FireHOLDataSQLite/test/.firehol-data-sqlite.ini')
        df = bla.get_ip_information('169.50.13.61')
        assert df.shape[0] == 2

        df = bla.get_ip_information('254.0.0.1')
        assert df.shape[0] == 0

    def test_network4(self):
        from BlockListAnalyzer import BlockListAnalyzer
        bla = BlockListAnalyzer('~/Git/Gitlab/OSINT-Tools/FireHOLDataSQLite/test/.firehol-data-sqlite.ini')
        df = bla.get_ip_information('0.1.2.3')
        assert df.shape[0] == 1

    def test_network6(self):
        from BlockListAnalyzer import BlockListAnalyzer
        bla = BlockListAnalyzer('~/Git/Gitlab/OSINT-Tools/FireHOLDataSQLite/test/.firehol-data-sqlite.ini')
        df = bla.get_ip_information('2001:218:3040:7e4:11e0:778:9ccc:61a2')
        assert df.shape[0] == 1

        df = bla.get_ip_information('2345:0425:2CA1:0:0:0567:5673:23b5')
        assert df.shape[0] == 1

    def test_full_db_network6(self):
        from BlockListAnalyzer import BlockListAnalyzer
        bla = BlockListAnalyzer('~/Git/Gitlab/OSINT-Tools/FireHOLDataSQLite/firehol-data-sqlite/.firehol-data-sqlite.ini')
        df = bla.get_ip_information('::1/128')
        assert df.shape[0] == 0

        df = bla.get_ip_information('192.168.10.0')
        assert df.shape[0] > 2
