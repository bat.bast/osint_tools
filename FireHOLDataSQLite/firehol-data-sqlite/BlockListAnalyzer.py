from configparser import ConfigParser
import os
import logging
from pathlib import Path
from datetime import datetime
import pandas
import sqlite3
import ipaddress


# See https://www.arsouyes.org/blog/2020/09_Geolocalisation_PHP_sqlite for IP convertion to int
# Reajusted for IPv6

class BlockListAnalyzer:
    def __init__(self, config: str = None):
        self.log_level = None
        self.feeds_directory = None
        self.database = None
        self._dataframes = None
        self._database_connection = None
        self.configure(config)

    def configure(self, config: str = None):
        if config is None:
            config = '.firehol-data-sqlite.ini'

        configuration = ConfigParser()
        if os.path.exists(os.path.expanduser(config)):
            configuration.read(os.path.expanduser(config))
        elif os.path.exists(os.path.join(os.path.expanduser("~"), config)):
            configuration.read(os.path.join(os.path.expanduser("~"), config))

        self.log_level = configuration.get('log', 'level', fallback='WARNING')
        self.feeds_directory = configuration.get('blocklist-ipsets', 'feeds_directory', fallback=None)
        self.database = configuration.get('database', 'filename', fallback=None)

    def init_database(self):
        if self.database is not None:
            # Delete old database
            database_file = os.path.expanduser(self.database)
            try:
                if os.path.exists(database_file):
                    os.remove(database_file)
            except Exception as ex:
                logging.error('Cannot delete existing database %s [KO]', database_file)
                logging.error('Error [%s] %s', type(ex), str(ex))
                exit(2)

    def fetch_database_connection(self):
        if self.database is not None:
            if self._database_connection is None:
                database_file = os.path.expanduser(self.database)
                try:
                    self._database_connection = sqlite3.connect(database_file)
                except Exception as ex:
                    logging.error('Cannot create database connection to %s [KO]', database_file)
                    logging.error('Error [%s] %s', type(ex), str(ex))
                    exit(1)

            return self._database_connection

    def analyze_blocklist(self):
        self.init_database()
        self.analyze_feeds()
        self.database_create_index()

    def database_create_index(self):
        logging.info('Creating database index...')
        datetime_start = datetime.now()

        tablename = 'FIREHOL_BLOCKLIST'
        for column_pre in ['IP_START', 'IP_END']:
            for column_suf in ['HIGH', 'LOW']:
                column = column_pre + '_' + column_suf
                self.fetch_database_connection().execute(
                    'CREATE INDEX ' + tablename + '_' + column + '_idx ON ' + tablename + ' (' + column + ')')
        for column in ['IP_VERSION', 'FEED_ID']:
            self.fetch_database_connection().execute(
                'CREATE INDEX ' + tablename + '_' + column + '_idx ON ' + tablename + ' (' + column + ')')
        tablename = 'FIREHOL_FEEDS'
        for column in ['CATEGORY', 'FEED_ID']:
            self.fetch_database_connection().execute(
                'CREATE INDEX ' + column + '_idx ON ' + tablename + ' (' + column + ')')

        datetime_delta = datetime.now() - datetime_start
        logging.info('Created database index in %s seconds [OK]', str(datetime_delta.total_seconds()))

    def analyze_feeds(self):
        logging.info('Analyzing feeds...')
        datetime_start = datetime.now()

        feeds_extensions = ['.ipset', '.netset']
        feeds_files = [p for p in Path(self.feeds_directory).expanduser().rglob('*') if p.suffix in feeds_extensions]
        list_feeds = []
        list_feeds_headers = ['FEED_ID', 'FEED_NAME', 'MAINTENER_URL', 'MAINTENER', 'LIST_SOURCE_URL',
                              'SOURCE_FILE_DATE', 'CATEGORY', 'ENTRIES']
        for feed_file in list(feeds_files):
            self.analyze_feed_file(feed_file, list_feeds)

        df_feeds = pandas.DataFrame(data=list_feeds, columns=list_feeds_headers)
        df_feeds.to_sql('FIREHOL_FEEDS', con=self.fetch_database_connection(), if_exists='replace', index=False)

        datetime_delta = datetime.now() - datetime_start
        logging.info('Analyzed feeds in %s seconds [OK]', str(datetime_delta.total_seconds()))

    def ipaddress_version(self, cidr: str) -> int:
        if ':' in cidr:
            return 6
        else:
            return 4

    def encode_int128_to_2xint64(self, src_int128: int) -> (int, int):
        src_high_int64 = src_int128 >> 64
        src_low_int64 = src_int128 & 0xffffffffffffffff
        return src_high_int64, src_low_int64

    def decode_2xint64_to_int128(self, src_high_int64: int, src_low_int64: int) -> int:
        src_int128 = (src_high_int64 << 64) | src_low_int64
        return src_int128

    def ipaddress_to_int(self, cidr: str) -> (int, int, int, int, int):
        try:
            network_address = ipaddress.ip_network(cidr)
            ip_version = 4 if isinstance(network_address, ipaddress.IPv4Network) else 6
            if ip_version == 4:
                cidr_start_high = None
                cidr_start_low = int(network_address[0])
                cidr_end_high = None
                cidr_end_low = int(network_address[-1])
            else:
                cidr_start_high, cidr_start_low = self.encode_int128_to_2xint64(int(network_address[0]))
                cidr_end_high, cidr_end_low = self.encode_int128_to_2xint64(int(network_address[-1]))

            return cidr_start_high, cidr_start_low, cidr_end_high, cidr_end_low, ip_version
        except ValueError:
            logging.error('IP %s is not a valid IP Address [KO]', cidr)
            return None, None, None, None, None

    def analyze_feed_file(self, file: Path, list_feeds: list):
        logging.debug('Analyzing feed file %s...', file.resolve())
        datetime_start = datetime.now()

        # Read comments meta informations
        feed_metadata = [len(list_feeds)]
        line_number = 0
        with open(file) as f:
            feed_strings = f.readlines()
            for feed_string in feed_strings:
                if line_number == 1:
                    feed_metadata.append(feed_string.split("# ")[1].strip())
                elif "Maintainer URL" in feed_string:
                    feed_metadata.append(feed_string.split(": ")[1].strip())
                elif "Maintainer" in feed_string:
                    feed_metadata.append(feed_string.split(": ")[1].strip())
                elif "List source URL" in feed_string:
                    feed_metadata.append(feed_string.split(": ")[1].strip())
                elif "Source File Date" in feed_string:
                    feed_metadata.append(feed_string.split(": ")[1].strip())
                elif "Category" in feed_string:
                    feed_metadata.append(feed_string.split(": ")[1].strip())
                elif "Entries" in feed_string:
                    feed_metadata.append(feed_string.split(": ")[1].strip())
                elif '#' not in feed_string:
                    break
                line_number += 1
            list_feeds.append(feed_metadata)

        # Read data into DataFrame
        df = pandas.read_csv(file, sep='\n', header=None, skiprows=line_number, names=['IP'])
        if df.shape[0] > 0:
            df['IP_START_HIGH'], df['IP_START_LOW'], df['IP_END_HIGH'], df['IP_END_LOW'], df['IP_VERSION'] = zip(
                *df['IP'].apply(self.ipaddress_to_int))
            df['FEED_ID'] = feed_metadata[0]

            df.to_sql('FIREHOL_BLOCKLIST', con=self.fetch_database_connection(), if_exists='append', index=False)

        datetime_delta = datetime.now() - datetime_start
        logging.debug('Analyzed feed file %s in %s seconds [OK]', file.resolve(),
                      str(datetime_delta.total_seconds()))

    def get_ip_information(self, ip: str) -> pandas.DataFrame:
        start_high, start_low, end_high, end_low, ip_version = self.ipaddress_to_int(ip)
        sql = 'SELECT FIREHOL_BLOCKLIST.IP, FIREHOL_BLOCKLIST.IP_VERSION, FIREHOL_FEEDS.*  FROM FIREHOL_BLOCKLIST JOIN FIREHOL_FEEDS' + \
              ' ON FIREHOL_BLOCKLIST.FEED_ID = FIREHOL_FEEDS.FEED_ID' + \
              ' WHERE' + \
              ' IP_VERSION = ' + str(ip_version) + \
              ' AND IP_START_LOW <= ' + str(start_low) + \
              ' AND ' + str(start_low) + ' <= IP_END_LOW'
        if ip_version == 6:
            sql += ' AND IP_START_HIGH <= ' + str(start_high) + \
                   ' AND ' + str(start_high) + ' <= IP_END_HIGH'
        return pandas.read_sql(sql, self.fetch_database_connection())


if __name__ == "__main__":
    analyzer = BlockListAnalyzer()
    logging.basicConfig(
        level=analyzer.log_level,
        format="[%(asctime)s - %(levelname)s] [%(filename)s:%(lineno)s - %(module)s.%(funcName)25s()] %(message)s"
    )

    if analyzer.feeds_directory is None:
        logging.error('Git directory is not set')
    elif analyzer.database is None:
        logging.error('Sqlite filename is not set')
    else:
        analyzer.analyze_blocklist()
