# OSINT Projets

## AnonymousProxy
Aggregate some sources to identify anonymous proxy IP

## FacebookDataBreach
Some analysis around Facebook Data Breach (Lucene, Solr)

## FireHOL
Fetch FireHOL sources 

## FireHOLDataSQLITE
Aggregate FireHOL sources into SQLite database (IPv6 ready)

## Maxmind
Aggregate Maxmind sources into SQLite database (IPv6 ready)

## NordVPN
Aggregate some sources to identify NordVPN IP

## Publish
Publish on my web server or Gitlab

