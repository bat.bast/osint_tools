#!/bin/bash

DIRNAME=$(dirname $0)

list=$(find $DIRNAME/../../*/data/ -type f)

cp $DIRNAME/../../NordVPN/data/nordvpn.domains.resolv.filtered.csv $DIRNAME/../../../Gitlab/osint/data/nordvpn/
cp $DIRNAME/../../AnonymousProxy/data/www.proxy-list.download.* $DIRNAME/../../../Gitlab/osint/data/anonymousproxy/

cd $DIRNAME/../../../Gitlab/osint/ && {
  git add *
  git commit -m $(date --iso-8601)
  git push --set-upstream origin master
}
