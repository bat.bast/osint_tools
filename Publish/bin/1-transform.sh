#!/bin/bash

DIRNAME=$(dirname $0)

jq '.data|select(has("answers") == true)|.answers[]|[.answer,.name]|@csv' $DIRNAME/../../NordVPN/data/nordvpn.domains.resolv.json | tr -d '"' | tr -d '\' > /dev/shm/nordvpn.csv 2>/dev/null

sort /dev/shm/nordvpn.csv > $DIRNAME/../../NordVPN/data/nordvpn.domains.resolv.filtered.csv
rm -f /dev/shm/nordvpn.csv
