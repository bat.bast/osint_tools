#!/bin/bash

DIRNAME=$(dirname $0)

test -d /var/www/www/OSINT || mkdir /var/www/www/OSINT

find /var/www/www/OSINT/*.7z -mtime 32 -type f -exec rm -f {} \;

list=$(find $DIRNAME/../../*/data/ -type f)
7z a -y /var/www/www/OSINT/$(date --iso-8601).7z $list 2>&1 >/dev/null

chown -R bast:www-data /var/www/www/OSINT
chmod -R 640 /var/www/www/OSINT/*
chmod 750 /var/www/www/OSINT
