#!/bin/bash

DIRNAME=$(dirname $0)

$DIRNAME/../../AnonymousProxy/bin/1-fetch_lists.sh
#$DIRNAME/../../NordVPN/bin/0-firehol-blockipset-pull-data.sh
$DIRNAME/../../NordVPN/bin/2-nordvpn_resolve.sh
$DIRNAME/../../Publish/bin/1-transform.sh
$DIRNAME/../../Publish/bin/2-publish_gitlab.sh >/dev/null
